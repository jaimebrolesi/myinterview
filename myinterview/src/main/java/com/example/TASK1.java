package com.example;

/**
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * <p>
 * <p>
 * <p>
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {

    public static void main(String[] args) {
        String racecar = "racecar";
        System.out.println(isPalindrome(racecar));
    }

    /**
     * Identifies if the given string is a palindrome.
     * I used n/2 to validate the string at middle. Verify each character from the begin at middle and from the end at middle, if all character are the same, it's a palindrome.
     *
     * @param validate string to be validated.
     * @return is palindrome or not.
     */
    private static boolean isPalindrome(String validate) {
        int n = validate.length();
        boolean result = Boolean.TRUE;
        for (int i = 0; i < (n / 2); ++i) {
            if (validate.charAt(i) != validate.charAt(n - i - 1)) {
                result = Boolean.FALSE;
            }
        }
        return result;
    }
}
