package com.example;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {

    /**
     * Default amount for distinct elements.
     */
    private static final int DEFAULT_SIZE = 0;

    /**
     * Execution method.
     * @param args jvm args.
     */
    public static void main(String[] args) {

        final List<String> objective = new ArrayList<String>() {{
            add("A"); add("B"); add("C"); // new
            add("A"); add("B"); add("C"); // duplicated
            add("D"); add("E"); add("F"); // new
        }};

        System.out.println(solution(objective));
    }


    /**
     * Solution for TASK 3.
     * @param objective list of String.
     * @return {@link Integer} how many distinct.
     */
    private static int solution(List<String> objective) {
        int result = 0;
        if (objective != null && !objective.isEmpty()) {
            final List<String> distinctList = objective.stream().distinct().collect(Collectors.toList());

            //print how many distinct
            result = distinctList.size();

            //print elements inside the list
            // result = distinctList.forEach(System.out::println);
        }
        return result;
    }
}
