package com.example;

/**
 * TASK2
 * Realizei uma implementação baseada na Linkedlist do Java
 *
 */
public class DoublyLinkedList<E> {

    private Node head;
    private Node tail;
    private int size;

    public DoublyLinkedList() {
        size = 0;
    }

    public E get(int index) {
        return node(index).element;
    }

    public E remove(int index) {
        return unlink(node(index));
    }

    private class Node {
        E element;
        Node next;
        Node prev;

        public Node(E element, Node next, Node prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "element=" + element +
                    '}';
        }
    }

    E unlink(Node x) {
        // assert x != null;
        final E element = x.element;
        final Node next = x.next;
        final Node prev = x.prev;

        if (prev == null) {
            head = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            tail = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }

        x.element = null;
        size--;
        return element;
    }

    Node node(int index) {
        if (index < (size >> 1)) {
            Node x = head;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node x = tail;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    public int size() { return size; }

    public boolean isEmpty() { return size == 0; }

    public void addFirst(E element) {
        Node tmp = new Node(element, head, null);
        if(head != null ) {head.prev = tmp;}
        head = tmp;
        if(tail == null) { tail = tmp;}
        size++;
    }

    public void addLast(E element) {

        Node tmp = new Node(element, null, tail);
        if(tail != null) {tail.next = tmp;}
        tail = tmp;
        if(head == null) { head = tmp;}
        size++;
    }

    public void print() {
        Node current = head;

        while(current != null) {
            System.out.println(current);
            current = current.next;
        }
    }

    public void removeMiddle() {
        int sum = 0;
        for(int i=1; i <= size; i++) {
            sum += i;
        }
        remove((sum/size) - 1);
    }

    public static void main(String a[]){

        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        dll.addFirst(1);
        dll.addFirst(2);
        dll.addLast(3);
        dll.addLast(4);
        dll.addLast(5);
        dll.addLast(6);
        dll.addLast(7);
        dll.print();
        dll.removeMiddle();
        dll.print();
    }
}
