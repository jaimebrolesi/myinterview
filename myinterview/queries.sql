-- quantidade de empregados por sexo

SELECT
  gender,
  count(*)
FROM employees
GROUP BY gender;

-- quantidade de empregados por sexo, ano de nascimento e ano de contratacao
SELECT
  count(*),
  gender,
  EXTRACT(YEAR FROM birth_date) birth_year,
  EXTRACT(YEAR FROM hire_date)  hire_year
FROM employees
GROUP BY gender, EXTRACT(YEAR FROM birth_date), EXTRACT(YEAR FROM hire_date);


-- media, min e max de salario por sexo
SELECT
  e.gender,
  avg(s.salary),
  min(s.salary),
  max(s.salary)
FROM salaries s
  JOIN employees e ON e.emp_no = s.emp_no
GROUP BY e.gender;



